package mis055.citrix.rule;

import org.apache.commons.lang3.StringUtils;

import static mis055.citrix.rule.WordType.consonants;
import static mis055.citrix.rule.WordType.digits;
import static mis055.citrix.rule.WordType.punctuation;
import static mis055.citrix.rule.WordType.space;
import static mis055.citrix.rule.WordType.vowel;

public class WordUtil {

    public static final WordType recognizedType(final String word) {

        if (StringUtils.isNotBlank(word)) {
            char ch = word.toLowerCase().charAt(0);
            if (ch == 'a' || ch == 'e' || ch == 'i' || ch == 'o' || ch == 'u') {
                return vowel;
            } else if ((ch >= 'a' && ch <= 'z')) {
                return consonants;
            } else if (ch >= '0' && ch <= '9') {
                return digits;
            } else if (isPunctuation(ch)) {
                return punctuation;
            } else throw new IllegalArgumentException("Unknown token type: " + word);

        } else {
            return space;
        }

    }

    public static boolean isPunctuation(char ch) {
        return ch == '.' || ch == ',' || ch == '!' || ch == '?' || ch == '\'' || ch == '’';
    }

    public static boolean isUpperCase(char ch) {
        return Character.isUpperCase(ch);
    }
}
