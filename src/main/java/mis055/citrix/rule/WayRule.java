package mis055.citrix.rule;

import org.apache.commons.lang3.mutable.MutableObject;

/**
 * Words that end in “way” are not modified.
 * <pre
 * stairway stays as stairway
 * </pre>
 */
public class WayRule implements Rule {

    @Override
    public void apply(MutableObject<String> word, RuleChain chain) {

        // if does not contain 'way' in the end then proceed with processing
        if (!chain.getOriginalWord().matches(".*way")) {
            chain.applyChain();
        }
    }
}
