package mis055.citrix.rule;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.mutable.MutableObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Rule transforms the Punctuation
 * <p>
 * Punctuation must remain in the same relative place from the end of the word.
 * <pre>
 *   ▪ can’t becomes antca’y
 *   ▪ end. becomes endway.
 * </pre>
 */
public class PunctuationRule implements Rule {

    @Override
    public void apply(MutableObject<String> word, RuleChain chain) {

        if (StringUtils.isNotBlank(word.getValue())) {

            final List<Integer> relativePosition = new ArrayList<>();
            final List<Character> punctuations = new ArrayList<>();
            final StringBuilder sb = new StringBuilder();

            // go thru the original word and mark the position of punctuation from the end
            final int length = chain.getOriginalWord().length();
            for (int i = 0; i < length; i++) {
                final char ch = chain.getOriginalWord().charAt(i);
                if (WordUtil.isPunctuation(ch)) {
                    relativePosition.add((length - 1) - i);
                    punctuations.add(ch);
                }
            }


            // skip all punctuations
            for (int i = 0; i < word.getValue().length(); i++) {
                final char ch = word.getValue().charAt(i);
                if (!WordUtil.isPunctuation(ch)) {
                    sb.append(ch);
                }
            }

            // add back punctuation to relative position
            for (int i = 0; i < relativePosition.size(); i++) {
                final int idx = (word.getValue().length() - 1) - relativePosition.get(i);
                final char punctuation = punctuations.get(i);
                sb.insert(idx, punctuation);
            }
            word.setValue(sb.toString());
        }

        chain.applyChain();
    }
}
