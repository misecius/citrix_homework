package mis055.citrix.rule;

import org.apache.commons.lang3.mutable.MutableObject;

/**
 * Words that start with a consonant have their first letter moved to the end of the word and the letters “ay” added to the end.
 * <pre>
 *     ▪Hello becomes Ellohay
 * </pre>
 */
public class ConsonantRule implements Rule {

    @Override
    public void apply(final MutableObject<String> word, final RuleChain chain) {

        final WordType wordType = WordUtil.recognizedType(chain.getOriginalWord());

        if (wordType == WordType.consonants) {
            final StringBuilder sb = new StringBuilder(word.getValue());
            sb.append(sb.charAt(0)).deleteCharAt(0).append("ay");
            word.setValue(sb.toString());
        }
        chain.applyChain();
    }
}
