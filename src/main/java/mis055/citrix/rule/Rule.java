package mis055.citrix.rule;

import org.apache.commons.lang3.mutable.MutableObject;

/**
 * Transformation rule which is applied to the given word
 */
public interface Rule {

    /**
     * Apply the transformation rule to the word
     *
     * @param word  word to be modified
     * @param chain chain of rules
     */
    void apply(MutableObject<String> word, RuleChain chain);
}
