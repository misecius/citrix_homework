package mis055.citrix.rule;

import org.apache.commons.lang3.mutable.MutableObject;

/**
 * Words that start with a vowel have the letters “way” added to the end.
 * <pre>
 *     apple becomes appleway
 * </pre>
 */
public class VowelRule implements Rule {

    @Override
    public void apply(final MutableObject<String> word, final RuleChain chain) {

        final WordType wordType = WordUtil.recognizedType(chain.getOriginalWord());

        if (wordType == WordType.vowel) {
            word.setValue(word.getValue() + "way");
        }
        chain.applyChain();
    }
}
