package mis055.citrix.rule;


import lombok.Getter;
import org.apache.commons.lang3.mutable.MutableObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;

public class RuleChain implements Cloneable {

    private final List<Rule> rules;
    private final AtomicInteger position;

    @Getter
    private final String originalWord;

    @Getter
    private final MutableObject<String> processingWord;

    public RuleChain(final String originalWord) {
        this.originalWord = originalWord;
        this.rules = new ArrayList<>();
        this.position = new AtomicInteger();
        this.processingWord = new MutableObject<>(this.originalWord);
    }

    /**
     * Method applies the the chain on the given word
     *
     * @return processed word
     */
    public String applyChain() {

        final Rule nextRule = getNextRule();

        if (nextRule != null) {
            nextRule.apply(processingWord, this);
        }

        return processingWord.getValue();

    }

    /**
     * Method register a new rule into the chain
     *
     * @param rule rule to by applied on the word
     */
    public RuleChain registerNewRule(final Rule rule) {
        Objects.requireNonNull(rule, "Rule cannot be null to be added into a chain");
        this.rules.add(rule);
        return this;
    }

    /**
     * Method register new rules into the chain
     *
     * @param rules rules to by applied on the word
     */
    public RuleChain registerNewRule(final List<Rule> rules) {
        Objects.requireNonNull(rules, "Rules cannot be null to be added into a chain");
        rules.forEach(this::registerNewRule);
        return this;
    }

    /**
     * Return all chain rules
     *
     * @return
     */
    public List<Rule> getRules() {
        return Collections.unmodifiableList(this.rules);
    }

    /**
     * Returns the next rule
     *
     * @return next rule or null if is exhausted
     */
    Rule getNextRule() {
        final int idx = this.position.getAndIncrement();
        if (idx < this.rules.size())
            return this.rules.get(idx);
        else
            return null;
    }

    /**
     * Creates the new chain for the new word. It copies all registered rules to new chain
     *
     * @param word word to precessed
     * @return a new chain
     */
    RuleChain newChain(final String word) {
        return new RuleChain(word).registerNewRule(this.getRules());
    }
}
