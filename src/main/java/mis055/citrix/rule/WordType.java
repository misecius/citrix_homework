package mis055.citrix.rule;

public enum WordType {
    vowel,
    consonants,
    digits,
    space,
    punctuation
}
