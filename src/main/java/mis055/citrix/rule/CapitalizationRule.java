package mis055.citrix.rule;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.mutable.MutableObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Rule capitalize the character at the given positions
 * <p>
 * Capitalization must remain in the same place.
 * <pre>
 * ▪ Beach becomes Eachbay
 * ▪ McCloud becomes CcLoudmay
 * </pre>
 */
public class CapitalizationRule implements Rule {

    @Override
    public void apply(MutableObject<String> word, RuleChain chain) {

        if (StringUtils.isNotBlank(word.getValue())) {

            final List<Integer> capitalPositions = new ArrayList<>();

            // go thru the original word and mark the position of capitalized characters from the end
            final int length = chain.getOriginalWord().length();
            for (int i = 0; i < length; i++) {
                if (WordUtil.isUpperCase(chain.getOriginalWord().charAt(i))) {
                    capitalPositions.add(i);
                }
            }

            // lower case all
            final StringBuilder sb = new StringBuilder(word.getValue().toLowerCase());

            // Capitalize back characters at the right position
            for (int idx : capitalPositions) {
                char ch = Character.toUpperCase(sb.charAt(idx));
                sb.deleteCharAt(idx);
                sb.insert(idx, ch);
            }
            word.setValue(sb.toString());
        }

        chain.applyChain();
    }
}
