package mis055.citrix.rule;

import org.apache.commons.lang3.mutable.MutableObject;

/**
 * Rule splits the given sentence into separate words and applies on each the chain
 */
public class SentenceRule implements Rule {

    @Override
    public void apply(final MutableObject<String> word, final RuleChain chain) {

        final String[] words = word.getValue().split("\\s+");
        final StringBuilder sb = new StringBuilder();

        if (words.length > 1) {

            for (int i = 0; i < words.length; i++) {
                final RuleChain newChain = chain.newChain(words[i]);
                final String processedWord = newChain.applyChain();
                sb.append(processedWord);
                if (i < (words.length - 1))
                    sb.append(" ");
            }
            word.setValue(sb.toString());
        } else {
            chain.applyChain();
        }
    }
}
