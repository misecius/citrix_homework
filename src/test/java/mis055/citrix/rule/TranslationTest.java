package mis055.citrix.rule;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

public class TranslationTest {

    final static List<Rule> rules = new ArrayList<>();

    @BeforeAll
    public static void init() {
        rules.add(new SentenceRule());
        rules.add(new HyphensRule());
        rules.add(new WayRule());
        rules.add(new VowelRule());
        rules.add(new ConsonantRule());
        rules.add(new PunctuationRule());
        rules.add(new CapitalizationRule());
    }

    public static Stream<Arguments> prepareData() {
        return Stream.of(
                Arguments.of("Ahoj Svete, jak-se veDe?", "Ahojway Vetesay, akjay-esay edEvay?"),
                Arguments.of("Can't do it!", "Antca'y oday itway!"),
                Arguments.of("Anyway the song 'Stairway to heaven' is best ever!!!", "Anyway hetay ongsay 'Stairway otay eavenhay' isway estbay everway!!!")
        );
    }

    @ParameterizedTest
    @MethodSource("prepareData")
    public void testTranslation(final String data, final String expected) {

        // given
        final RuleChain chain = new RuleChain(data).registerNewRule(rules);

        // when
        final String result = chain.applyChain();

        // then
        assertThat(result).isEqualTo(expected);
    }

}
