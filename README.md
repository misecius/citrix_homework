# Pig latin

The design is based on the _**chain-of-responsibilities pattern**_. Each transformation rule is separate Rule instance which is responsible to do or not the 
transformation of the incoming string 
 
Sources are location obviously in _src/main/java_ folder
There is prepared on test in _src/test/java_ folder which tests the transformation
